var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

/*
var index = require('./routes/index');
var users = require('./routes/users');
*/

var dbURL = "mongodb://dbvash:blackops2@ds137019.mlab.com:37019/mypage?authMechanism=SCRAM-SHA-1";

const db = require('monk')(dbURL);

var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken');


const personajes = db.get('Personajes');
const users = db.get('users');

var opts = {};

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
opts.secretOrKey = "OBLIVION";

passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
    console.log(jwt_payload)
    users.findOne({"_id": jwt_payload._id})
        .then(function (user) {
            if (user) {
                done(null, true);
            } else {
                done(null, false);
            }
        }).catch(function (error) {
        console.log("ERROR " + error)
    });
}));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());

/*
app.use('/', index);
app.use('/users', users);
*/

app.post('/api/auth/register', function (req, res) {
    var data = req.body;

    if (!data.username || !data.password) {
        res.json({success: false, msg: 'Por favor, introduce el usuario y la contraseña.'});
    } else {
        users.findOne({username: data.username})
            .then(function (user) {
                console.log(user);

                if (!user) {
                    users.insert(data);
                    return res.json({success: true, msg: 'Usuario creado.'});
                } else {
                    return res.json({success: false, msg: 'El usuario ya existe. '});
                }
            });
    }
});

app.post('/api/auth/login', function (req, res) {
    var data = req.body;

    users.findOne({username: data.username})
        .then(function (user) {
            console.log(user);
            if (!user) {
                res.status(401).send({success: false, msg: 'Autenticación fallida. Usuario no encontrado.'})
            } else {
                if (user.password === data.password) {
                    var token = jwt.sign(
                        {
                            "_id": user._id,
                            "username": user.username
                        },
                        opts.secretOrKey
                    );
                    //Devuelve la información incluyendo el token como JSON
                    res.json({success: true, token: 'JWT ' + token});
                } else {
                    res.status(401).send({success: false, msg: 'Autenticación fallida. Contraseña errónea. '});
                }
            }
        })
});

app.get('/rss', function (req, res) {
    db.select().from("Personajes")
        .then(function (data) {
            console.log(data);
            var rss =
                `<?xml version="1.0" encoding="UTF-8" ?>
                <rss version="2.0">

                <channel>
    <title>Los Vengadores: La era de Ultron</title>
    <link>localhost</link>
    <description>Personajes</description>`;


            console.log(data.length);
            for (character of data) {

                rss += `
                <item>
                    <title>${character.Nombre_personaje}</title>
                    <link>${character.img}</link>>
                    <description>${character.Perfil}</description>
                </item>`;
            }

            rss += `
  </channel>
</rss>`;

            res.set('Content-Type', 'text/xml');
            res.send(rss);
        }).catch(function (error) {
        console.log(error);
    });
});


app.get('/api/vengadores', function (req, res) {
    personajes.find({}).then(function (data) {
        res.json({Personajes: data});
    }).catch(function (error) {
        console.log(error);
    })
});

app.get('/api/mensajes', function (req, res) {
    db.select().from("mensajes").then(function (data) {
        res.json({mensajes: data});
    }).catch(function (error) {
        console.log(error);
    })
});

app.get('/api/vengadores/:id', function (req, res) {
    var _id = req.params.id;

    personajes.find({"_id": _id})
        .then(function (data) {
            res.json(data);
        }).catch(function (error) {
        console.log(error);
    })
});

app.post('/api/vengadores', function (req, res) {
    var data = req.body;

    personajes.insert(data).then(function (data) {
        res.json({Personajes: data});
    }).catch(function (error) {
        console.log(error);
    })
});

app.post('/api/mensajes', function (req, res) {
    var data = req.body;

    db.insert(data).into("mensajes").then(function (data) {
        res.json({mensajes: data});
    }).catch(function (error) {
        console.log(error);
    })
});

app.post('/api/vengadores/:id', function (req, res) {
    var _id = req.params.id;
    var data = req.body;

    personajes.update({"_id": _id}).where("personaje_id", id)
        .then(function (data) {
            res.json(data);
        }).catch(function (error) {
        console.log(error);
    })
});

app.delete('/api/vengadores/:id', passport.authenticate('jwt', {session: false}), function (req, res) {
    if(req.user) {
        console.log(req.user.username)

        var _id = req.params.id;

        console.log(_id)
        personajes.remove({_id: _id})
            .then(function (data) {
                console.log("ENTRA")

                res.json(data);
            }).catch(function (error) {
            console.log("ERROR " + error);
        });
    }else{
        return res.status(403).send({success: false, msg: 'No autorizado.'})
    }
});

app.delete('/api/mensajes/:id', function (req, res) {
    var id = parseInt(req.params.id);

    db.delete().from("mensajes").where("id_mensaje", id)
        .then(function (data) {
            res.json(data);
        }).catch(function (error) {
        console.log(error);
    })
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
